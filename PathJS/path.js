

(function (){
var pathLibrary = {};
var LoggingUtils = {};

LoggingUtils.getUserInfo = function getUserInfo() {
    var logData = {
        UserAgent: navigator.userAgent,
        UserScreenWidth: screen.availWidth,
        UserScreenHeight: screen.availHeight,
        UserLanguage: navigator.language
    }
    return logData;
}
LoggingUtils.getActionInfo = function getActionInfo(event) {
    var actionLog = {
        date: new Date(),
        type: event.type,
        element: event.target.tagName,
        attributes: event.target.attributes

    };
    // console.log(event.key);
    return actionLog;
}

if (window.PathJS) {    
    throw new Error("PathJS already loaded");

} else {
    pathLibrary.loggingUtils = LoggingUtils;
    window.PathJS = pathLibrary;
}})();


