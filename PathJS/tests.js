// (function() {
//     var assert = window.chai.assert;
//     var PathJS = window.PathJS;
//     assert.exists(PathJS, "PathJS is not loaded");
//     assert.equal(PathJS.validators.isNumber(3), true, "Expected number 3 to be true");
//     assert.equal(PathJS.validators.isNumber("3"), false, "Expected string to be false");
//     assert.equal(PathJS.validators.isNumber(2.2222222), true, "Expected number 2.222222 to be true");
//     assert.equal(PathJS.validators.isNumber(NaN), false, "Expected NaN to be false");
//     assert.equal(PathJS.validators.isNumber(Infinity), false, "Expected Infinity to be false");
//     assert.equal(PathJS.validators.isString("TEST"), true, "Expected String to be True");
//     assert.equal(PathJS.validators.isString(""), true, "Expected String to be True");
//     assert.equal(PathJS.validators.isString(" "), true, "Expected String to be True");
//     assert.equal(PathJS.validators.isString(3), false, "Expected String to be False"); 
//     assert.equal(PathJS.validators.isString(true), false, "Expected String to be False");
//     assert.equal(PathJS.validators.isBoolean("string"), false, "Expected Boolean to be False");
//     assert.equal(PathJS.validators.isBoolean(true), true, "Expected Boolean to be True"); 
//     assert.equal(PathJS.validators.isBoolean(false), true, "Expected Boolean to be True");
//     assert.equal(PathJS.validators.isNull(null), true, "Expected Null to be Null"); 
//     assert.equal(PathJS.validators.isNull(undefined), false, "Expected Null to be False");
//     assert.equal(PathJS.validators.isNull(false), false, "Expected Null to be False");
//     assert.equal(PathJS.validators.isUndefined(null), false, "Expected Undefined to be False");
//     assert.equal(PathJS.validators.isUndefined(undefined), true, "Expected Undefined to be True"); 
//     assert.equal(PathJS.validators.isUndefined(false), false, "Expected Undefined to be False");
//     assert.equal(Number(2).isBetween(1,5), true, "Expected true test");
//     assert.equal(PathJS.numberUtils.isBetween(2, 1, 5), true);
//     assert.equal(Number(2).isBetween(2,2), true, "Expected true test2");
//     assert.equal(Number(2).isBetween(4,5), false, "Expected false test");
//     assert.equal(Number(NaN).isBetween(-Infinity, Infinity), false, "Expected infinity false");
//     assert.equal(Number(2).isBetween(33, 2), false, "Expectez 2 false");

//     assert.exists(Number.getRandomInteger(0,5), 1);
//     assert.equal(Number.getRandomInteger(0,5).isBetween(0,5), true, 2);
//     assert.exists(PathJS.numberUtils.getRandomInteger(0,5));
//     assert.equal(Number.getRandomInteger(Number.MIN_VALUE, Number.MAX_VALUE).isBetween(Number.MIN_VALUE, Number.MAX_VALUE), true, 3);
//     assert.notExists(Number.getRandomInteger("a","b"), 4);

//     assert.exists(Number.getRandomFloat(0,5));
//     assert.exists(PathJS.numberUtils.getRandomFloat(0,5));
//     assert.equal(Number.getRandomFloat(0,5).isBetween(0,5), true);
//     assert.equal(Number.getRandomFloat(Number.MIN_VALUE, Number.MAX_VALUE).isBetween(Number.MIN_VALUE, Number.MAX_VALUE), true);
//     assert.notExists(Number.getRandomFloat("a","b"));

//     assert.exist(Date.dateFormObject);
//     assert.equal(Date.dateFormObject({year: 2019, month: 'February', day: 15, hours: 14, minutes: 00, seconds: 00}), new Date(2019, 1, 15, 14, 0, 0));
//     assert.equal(Date.dateFormObject({hours: 14, minutes: 0}), new Date(0, 0, 0, 14, 0, 0));
//     assert.equal(Date.dateFormObject({year: 'Default', month: 'February', day: 15, hours: 14, minutes: 0, seconds: 0}),undefined);
//     // assert.equal(Date.dateFormObject({year: 2019, month: 2, day: 15, hours: 14, minutes: 0, seconds: 0)}, undefined);   
//     assert.equal(Date.dateFormObject({year: 0, month: '', day: 0, hours: 0, minutes: 0, seconds: 0}), undefined);

// })();
function logAction(event) {
    return event;
}
